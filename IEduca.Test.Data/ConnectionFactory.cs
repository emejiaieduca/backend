﻿using IEduca.Test.Transversal.Common;
using System;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace IEduca.Test.Infrastructure.Data
{
    public class ConnectionFactory : IConnectionFactory
    {
        private readonly IConfiguration _configuration;

        public ConnectionFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IDbConnection GetConnection
        {
            get
            {
                var sqlconnection = new SqlConnection();
                if (sqlconnection == null) return null;

                sqlconnection.ConnectionString = _configuration.GetConnectionString("NorthwinConnection");
                sqlconnection.Open();
                return sqlconnection;
            }
        }
    }
}
