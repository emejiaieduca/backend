﻿using System;
using AutoMapper;
using IEduca.Test.Domain.Entity;
using IEduca.Test.Application.DTO;

namespace IEduca.Test.Transversal.Mapper
{
    public class MappingsProfile : Profile
    {
        public MappingsProfile()
        {
            CreateMap<Client, ClientDTO>().ReverseMap();
        }
    }
}
