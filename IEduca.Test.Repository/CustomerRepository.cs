﻿using System;
using System.Collections.Generic;
using IEduca.Test.Domain.Entity;
using IEduca.Test.Infrastructure.Interface;
using IEduca.Test.Transversal.Common;
using Dapper;
using System.Data;
using System.Threading.Tasks;

namespace IEduca.Test.Infrastructure.Repository
{
    public class ClientRepository :IClientRepository
    {
        private readonly IConnectionFactory _connectionFactory;

        public ClientRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public async Task<bool> Delete(string clientId)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "ClientsDelete";
                var parameters = new DynamicParameters();
                parameters.Add("ClientID", clientId);
                var result = connection.Execute(query, param: parameters, commandType: CommandType.StoredProcedure);
                return result > 0;
            }
        }

        public async Task<Client> Get(string clientId)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "ClientsGetByID";
                var parameters = new DynamicParameters();
                parameters.Add("ClientID", clientId);

                var client = connection.QuerySingle<Client>(query, param: parameters, commandType: CommandType.StoredProcedure);
                return client;
            }
        }

        public async Task<IEnumerable<Client>> GetAll()
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "ClientsList";
                var clients = connection.Query<Client>(query, 
                    commandType: System.Data.CommandType.StoredProcedure);
                return clients;
            }
            
        }

        public async Task<bool> Insert(Client client)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "ClientsInsert";
                var parameters = new DynamicParameters();
                parameters.Add("ClientID", client.ClientId);
                parameters.Add("Name", client.Name);
                parameters.Add("ContactName", client.Lastname);
                parameters.Add("ContactTitle", client.Phone);
                parameters.Add("Address", client.Address);
                parameters.Add("City", client.Email);

                var result = connection.Execute(query, param: parameters, commandType: CommandType.StoredProcedure);
                return result > 0;
            }
        }

        public async Task<bool> Update(Client client)
        {
            using (var connection = _connectionFactory.GetConnection)
            {
                var query = "ClientsUpdate";
                var parameters = new DynamicParameters();
                parameters.Add("ClientID", client.ClientId);
                parameters.Add("Name", client.Name);
                parameters.Add("ContactName", client.Lastname);
                parameters.Add("ContactTitle", client.Phone);
                parameters.Add("Address", client.Address);
                parameters.Add("City", client.Email);

                var result = connection.Execute(query, param: parameters, commandType: CommandType.StoredProcedure);
                return result > 0;
            }
        }
    }
}
