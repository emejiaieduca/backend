﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IEduca.Test.Domain.Entity
{
    public class Client
    {
        public string ClientId { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
    }
}
